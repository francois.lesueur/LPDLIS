Application web de gestion clientèle.

- admin/ contient le code pour ajouter/supprimer des clients
- helpdesk/ contient le code pour modifier des clients, lire leur message et le modifier
- clients/ contient le code permettant à chaque client de voir ses infos et d'envoyer un message au helpdesk

Dans chaque dossier :
- index.php fournit la page de login
- do_login.php traite le login, stocke login et pass dans la session PHP puis redirige vers la page d'affichage print_clients.php
- print_clients.php affiche, selon le cas, la liste + ajout/suppression, la liste + modification ou le compte du client connecté
- do_*.php traite les actions sur la BD (insertion, suppression, modification) demandées par print_clients.php puis redirige vers print_clients.php

La table utilisée est 'clients' de la base 'clientsdb' avec le compte www/foo.
