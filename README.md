# LPDLIS

Authentification et sécurité des données pour les LP DLIS - IUT Vannes

* [Cours Cryptographie (2*1h)](cours-crypto.md)
* [TD crypto asymétrique (2h)](td-crypto.md)
* [Passwords (2h)](td-passwords.md)
* [TP HTTPS / Authentification mutuelle (beaucoup d'heures)](tp-https.md)
* [TP LDAP (plein d'heures)](tp-ldap.md)
