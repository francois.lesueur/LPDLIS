# Cours Authentification forte (1h)


Mode d'emploi
=============

Déroulement du cours en "autonomie encadrée" :

* Vous avez un travail en autonomie décrit dans la suite de ce document. Ce document vous oriente vers des références externes (à lire, évidemment) et tisse les liens entre ces lectures.
* Durant tout ce travail, je suis disponible autant pour des éclaircissements que des approfondissements.
* Je vous encourage, évidemment, à échanger aussi en petits groupes via le canal de votre choix
* Les durées sont données à titre indicatif et seront très variables en fonction de vos questions.

Ce que nous allons voir :

* Le principe de l'authentification, l'authentification multi-facteurs et l'authentification forte
* La suite se concentrera sur l'authentification forte par certificat numérique


L'authentification forte (1 heure)
========================

En informatique, on parle de :

* Identification : association d'un identifiant à un utilisateur
* Authentification : contrôle de cette association (c'est ici que l'on parle de vérification de cet identifiant). Il existe plusieurs méthodes classiques pour cela, décrites [ici](https://fr.wikipedia.org/wiki/Authentification)

L'authentification est couramment un point faible de la sécurité des systèmes. De nombreuses attaques tentent d'usurper des comptes, soit via du phishing pour obtenir des authentifiants valides soit via des attaques directes. Intuitivement, ce mécanisme étant le point d'entrée dans le système et un de ses gardiens essentiels, il est évidemment critique, d'autant plus pour l'authentification des comptes à privilèges (administrateurs).

Afin de rendre l'authentification plus robuste, les authentifications multi-facteurs ou fortes permettent de réduire les risques. Une petite lecture [ici](https://fr.wikipedia.org/wiki/Authentification_forte). L'ANSSI fait la distinction entre les 2 (et apporte en tous cas un élément de réflexion et de choix) dans la [Section 2.5 du guide de l'authentification](https://www.ssi.gouv.fr/uploads/2021/10/anssi-guide-authentification_multifacteur_et_mots_de_passe.pdf#section.2.5).

Pour la suite, nous explorerons le certificat numérique comme facteur cryptographique.
